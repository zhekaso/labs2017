
/*7
Расположить строки по возрастанию количества цифр
Входные параметры:
1. Массив
2. Размерность массива
Выходные параметры:
1. Количество цифр
2. Вторая цифра строки*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <mcheck.h>

#define MAX_LEN 1024

int countNumbers(const char* s)
{
	int count = 0;
	for(int i = 0; s[i] != '\0'; i++)
		if(s[i] >= '0' && s[i] <= '9')
			count++;
	return count;
}

char secondNum(const char* s)
{
	char c = '-';
	for(int i = 0, n = 0; s[i] != '\0' && n < 2; i++)
		if(s[i] >= '0' && s[i] <= '9')
		{
			n++;
			if(n==2) c = s[i];
		}
		return c;
}

int cmp(const void* s1, const void* s2)
{
	int count1 = countNumbers(*(char**)s1);
	int count2 = countNumbers(*(char**)s2);
	if(count1 > count2) return 1;
	if(count1 < count2) return -1;
	return 0;
}

void freeMemory(int N, char **memory)
{
	for(int i = 0; i < N; i++)
	{
		free(memory[i]);
	}
	free(memory);
}
int inp_str(char** string, int maxlen)
{
	char buff_str[MAX_LEN];
	char buff_format[MAX_LEN];
	sprintf(buff_format,"%%%ds",maxlen);
	scanf(buff_format, buff_str);
	int len = strlen(buff_str);
	*string=malloc(sizeof(char*)*len);
	strcpy(*string,buff_str);
	return len;
}
char **readStrings(int N)
{
	char** memory;
	memory = calloc(N,sizeof(char *));
	printf("Введите строки\n");
	for(int i = 0; i < N; i++)
	{
		inp_str(&memory[i],MAX_LEN);
	}
	return memory;
}
void out_str(char* string, int length, int number)
{

}
void writeStrings(int N, char** strings)
{
	printf("\nОтсортированные строки\n");
	printf("Строка  |  Количество цифр  |  Вторая цифра строки\n");
	for(int i = 0; i < N; i++)
	{
		int count = countNumbers(strings[i]);
		char c = secondNum(strings[i]);
		printf("%s\t\t   %d\t\t     %c\n",strings[i],count,c);
	}
}

int main()
{
	mtrace();
	char **strings;
	int count;
	printf("Введите колличество строк: ");
	scanf("%d", &count);
	strings = readStrings(count);
	qsort(strings,count,sizeof(char*),cmp);
	writeStrings(count,strings);
	freeMemory(count,strings);
	return 0;
}
