
/*Фамилия
Группа
Номер в списке
Стипендия
Расположить записи в порядке возрастания номера в списке */

#include <stdio.h>
#include <stdlib.h>

#define MAX_LEN 1024

struct student
{
int number;
char name[MAX_LEN];
char grup[MAX_LEN];
int grant;
};
typedef struct student s_student;

int cmp(const void *s1, const void *s2)
{
	s_student st1 = *(s_student*)s1;
	s_student st2 = *(s_student*)s2;
	if(st1.number > st2.number) return 1;
	if(st1.number < st2.number) return -1;
	return 0;
}



s_student *readStudents(int count)
{
	s_student *memory;
	memory = calloc(count,sizeof(s_student));
	printf("Введите имена в формате:\nномер фамилия группа стипендия\n" );
	for(int i = 0; i < count; i++)
	{
		scanf("%d %s %s %d", &memory[i].number,memory[i].name,memory[i].grup,&memory[i].grant);
	}
	return memory;
}

void printStudents(int count, s_student *students)
{
	printf("Список студентво:\n");
	for(int i = 0; i < count; i++)
	{
		printf("%d %s %s %d\n", students[i].number,students[i].name,students[i].grup,students[i].grant);
	}
}

void freeMemory(s_student *memory)
{
	free(memory);
}

int main()
{
	s_student *students;

	int count;
	printf("Введите колличество студентов: ");
	scanf("%d",&count);
	students = readStudents(count);
	qsort(students,count,sizeof(s_student),cmp);
	printStudents(count,students);
	freeMemory(students);
	return 0;
}
