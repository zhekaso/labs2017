
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NAME_LEN 64
#define MAX_LEN 1024

//Меняет имя тип файла на "mod"
void changeType(char* new_name,char* old_name)
{
	int i;
	for(i = 0; old_name[i] != '.' && old_name != '\0'; i++)
	{
		new_name[i] = old_name[i];
	}
	new_name[i] = '.';
	i++;
	strcpy(&new_name[i], "mod\0");
}

//Проверяет последний символ строки, если цифра возращает 1
int lastSymbolNum(char *line)
{
	int len = strlen(line);
	char c = line[len - 1];
	if(c >= '0' && c <= '9')
		return 1;
	return 0;
}

//Читает строки из файл с именем fin, если заканцивается цифрой переносит в файл fout
void modifiFile(char* fin,char* fout)
{
	FILE *fw,*fr;
	if( (fr = fopen(fin, "r")) == NULL )
	{
		printf("Не удалось открыть файл %s!\n",fin);
		exit(1);
	}
	if( (fw = fopen(fout, "w")) == NULL )
	{
		printf("Не удалось открыть файл %s!\n",fout);
		fclose(fr);
		exit(1);
	}
	char buf[MAX_LEN];
	while(fscanf(fr," %[^\n]",buf) > 0)
	{
		fgetc(fr);
		if(lastSymbolNum(buf))
		{
			fprintf(fw, "%s\n", buf);
		}
	}
	fseek(fw, -1, SEEK_CUR);
	fprintf(fw, "%c", '\0');
	fclose(fw);
	fclose(fr);
}
int main(int argc, char* argv[])
{

	if(argc < 2)
		{
			printf("Ошибка: запускать %s имя_файла max_длина_строки\n",argv[0]);
			exit(1);
		}
	char fname[NAME_LEN];
	changeType(fname,argv[1]);
	modifiFile(argv[1],fname);
	return 0;
}
