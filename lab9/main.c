
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <fcntl.h>
#include <sys/types.h>
//#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>

#define CODE_FILE "/tmp/code_l8"
#define MAX_MSG_LEN 1024
#define SHM_SIZE 2*sizeof(int)
//#define _SVID_SOURCE
typedef struct
{
  long mtype;
  int msg[2];
} s_msg;

int openSHM(key_t key)
{
  int id = shmget(key, 0, IPC_EXCL);
  if(id == -1)
  {
    printf("Не открыть сегмент\n");
    exit(1);
  }
  return id;
}
int createSHM(key_t key)
{
  int id = shmget(key,SHM_SIZE,IPC_CREAT |  0666);
  if(id == -1)
  {
    printf("Не удалось создать сегмент\n");
    exit(1);
  }

  return id;
}
int *getSHM(int id)
{
  int *shm = (int *)shmat(id, NULL, 0);
  if(shm == (int*)-1)
  {
    printf("Не удалось получить адресс сегмента\n");
    exit(1);
  }
  return shm;
}
void rmSHM(int id)
{
  if(shmctl(id, IPC_RMID, 0) < 0)
  {
    printf("Не удалось закрыть разделяемую память\n");
  }
}

int getVAL(int id)
{
  return semctl(id, 0, GETVAL, 0);
}
int createSEM(key_t key)
{
  int id = semget(key, 1, IPC_CREAT |  0666);
  if(id == -1)
  {
    printf("Не удалось создать семафор\n");
    exit(1);
  }

  semctl(id, 0, SETVAL, 0);
  return id;
}

int openSEM(key_t key)
{
  int id = semget(key, 0, IPC_EXCL);
  if(id == -1)
  {
    printf("Не удалось открыть семафор\n");
    exit(1);
  }
  else return id;
}

void P(int semID)
{
  struct sembuf lock_res = {0, -1, 0}; //блокировка ресурса
  semop(semID, &lock_res, 1);
}

void W(int semID)
{
  struct sembuf wait_res = {0, 0, 0}; //блокировка ресурса
  semop(semID, &wait_res, 1);
}
void V(int semID)
{
  struct sembuf rel_res = {0, 1, 0}; //освобождение ресурса
  semop(semID, &rel_res, 1);
}

void rmSEM(int id)
{
  semctl(id, 0, IPC_RMID);
}

int nod(int a, int b)
{
	while(a != b)
	{
		if(a > b)
			a = a - b;
		else b = b - a;
	}
	return a;
}

int primenumber(int num)
{
	for(int i = 2; i <= (int)sqrt(num) + 1;i++)
	{
		int NOD = nod(num,i);
		if( NOD != 1 && NOD != num) return 0;
	}
	return 1;
}

int main(int argc,char* argv[])
{

	if(argc < 4)
	{
		printf("Введите: %s начало_диапозона конец_диапозона max_чисел_в_диапозоне\n",argv[0]);
		exit(1);
	}
	//диапозон [a;b]
	int a = atoi(argv[1]);
	int b = atoi(argv[2]);
	//n - количество чисел анализируемых в 1 процессе
	int n = atoi(argv[3]);
	int s,e,count = 0;
	s = a;
  unlink(CODE_FILE);
  close(creat(CODE_FILE, 0666));
  int semID = createSEM(ftok(CODE_FILE, 2));
  int shmID = createSHM(ftok(CODE_FILE, 3));
  int *data = getSHM(shmID);

	do
	{
		e = s + n - 1;
		if( e > b) e = b;
		count++;

    W(semID); //Если семафор - 0 можно писать
    data[0] = s;
    data[1] = e;
    V(semID); //семафор=1

		if(!fork())
		{
			int s,e;
      int semID = openSEM(ftok(CODE_FILE, 2));
      int shmID = openSHM(ftok(CODE_FILE, 3));
      int *data = getSHM(shmID);

      //семафор - 1 можно читать
      s = data[0];
      e = data[1];
      P(semID);//ceмафор=0
      //printf("\n[%d : %d]\n",s,e);
      shmdt(data);

			for(int i = s; i <= e; i++)
			{
				if(primenumber(i)) printf("%d ",i);
			}

			exit(0);
		}
		s = e + 1;
	} while(e < b);

	for(int i = 0; i < count; i++)
		wait(NULL);

  rmSEM(semID);
  rmSHM(shmID);
	printf("\n");
	return 0;
}
