
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <pthread.h>

int nod(int a, int b)
{
	while(a != b)
	{
		if(a > b)
			a = a - b;
		else b = b - a;
	}
	return a;
}

int primenumber(int num)
{
	for(int i = 2; i <= (int)sqrt(num) + 1;i++)
	{
		int NOD = nod(num,i);
		if( NOD != 1 && NOD != num) return 0;
	}
	return 1;
}

static pthread_mutex_t lock=PTHREAD_MUTEX_INITIALIZER;

void *find_primenumbers(void * in)
{
	int *data = (int *) in;
	int s = data[0];
	int e = data[1];
	pthread_mutex_unlock(&lock);
	for(int i = s; i <= e; i++)
	{
		if(primenumber(i)) printf("%d ",i);
	}
	//printf("[%d ; %d]\n",s,e);
	return NULL;
}

int main(int argc,char* argv[])
{
	if(argc < 4)
	{
		printf("Введите: %s начало_диапозона конец_диапозона max_чисел_в_диапозоне\n",argv[0]);
		exit(1);
	}
	//диапозон [a;b]
	int a = atoi(argv[1]);
	int b = atoi(argv[2]);
	//n - количество чисел анализируемых в 1 процессе
	int n = atoi(argv[3]);
	int s,e,count = (b - a + 1) / n;
	if ((b - a + 1) % n) count++;

	pthread_t *tid = calloc(count, sizeof(pthread_t));
	/*pthread_mutex_t mid;
	pthread_mutex_init(&mid, const pthread_mutexattr_t *__mutexattr)*/
	int *data = calloc(2,sizeof(int));
	int k = 0;
	s = a;
	do
	{
		e = s + n - 1;
		if( e > b) e = b;
		pthread_mutex_lock(&lock);
		pthread_mutex_unlock(&lock);
		data[0] = s;
		data[1] = e;
		pthread_mutex_lock(&lock);
		if(pthread_create(&tid[k], NULL, find_primenumbers, data))
		printf("Не удалось создать поток\n");
		k++;
		s = e + 1;
	} while(e < b);
	for(int i = 0; i < count; i++)
		pthread_join(tid[i], NULL);
	//printf("\nForks: %d\n",count);
	printf("\n");
	free(tid);
	free(data);
	return 0;
}
