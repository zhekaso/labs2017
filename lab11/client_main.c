#define _BSD_SOURCE
#include <unistd.h>

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <string.h>
#include "conf.h"
union msg
{
  int i;
  char c[sizeof(int)];
};

union msg2
{
  int i[2];
  char c[2*sizeof(int)];
};

int nod(int a, int b)
{
	while(a != b)
	{
		if(a > b)
			a = a - b;
		else b = b - a;
	}
	return a;
}

int primenumber(int num)
{
	for(int i = 2; i <= (int)sqrt(num) + 1;i++)
	{
		int NOD = nod(num,i);
		if( NOD != 1 && NOD != num) return 0;
	}
	return 1;
}

int openSocket(char* addr,int port)
{
  int sd = socket(AF_INET, SOCK_STREAM, 0);
  if(sd < 0)
  {
    printf("Не удалось созадть сокет\n");
    exit(1);
  }
  struct sockaddr_in serv_addr;
	memset(&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_addr.s_addr = inet_addr(addr);
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);

  if(connect(sd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0)
  {
    printf("Не удалось присоединится к серверу\n");
    exit(1);
  }
  return sd;
}

int main()
{
  int sd = openSocket("127.0.0.1", PORT_TCP);
  union msg2 indata;
  union msg outdata;
  //Получаем диапозон [a ; b]
  recv(sd, indata.c, 2 * sizeof(int), 0);

  //Анализирум диапозон на простые числа
  for(int i = indata.i[0]; i <= indata.i[1]; i++)
  {
    if(primenumber(i) == 1)
		{
			outdata.i=i;
      send(sd, outdata.c, sizeof(int), 0);
		}
  }

  outdata.i = -1;
  //Посылаем код завершения серверу
  send(sd, outdata.c, sizeof(outdata.i), 0);
  //usleep(100);
  close(sd);
  return 0;
}
