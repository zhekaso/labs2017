
#define _BSD_SOURCE
#include <unistd.h>

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>

#include "conf.h"

#define Q_LEN 10


int openSocket(int port,int qlen)
{
  int sd = socket(AF_INET, SOCK_STREAM, 0);
  if(sd < 0)
  {
    printf("Не удалось созадть сокет\n");
    exit(1);
  }
  //Задаём адрес и порт сокета
  struct sockaddr_in serv_addr;

  memset(&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);

  if(bind(sd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
  {
    printf("Cервер:Не удалось привязать порт %d сокету\n",port);
    close(sd);
    exit(1);
  }

  if(listen(sd,qlen) < 0 )
  {
    printf("Не удалось начать слушать порт\n");
    exit(1);
  }
  return sd;
}
typedef struct {
  int sock;
  int s;
  int e;
} thread_data;

//Функция потока для общения с клиентами
void *fthread(void *in)
{
  thread_data td = *((thread_data*)in);
  int data[2];
  data[0] = td.s;
  data[1] = td.e;
  //Посылаем диапозон для клиента
  send(td.sock, data, 2 * sizeof(int), 0);
  //Получаем простые числа из диапозона
  do
  {
    recv(td.sock,data, sizeof(int), 0);
    usleep(15);
    if(data[0] != -1)
    {
      printf("%d ", data[0]);
    }

  } while(data[0] != -1);

  //Закрываем сокет
  usleep(100);
  if(close(td.sock))
  {
    printf("Не удалось закрыть сокет клиента\n");
  }
  return NULL;
}


int main(int argc,char* argv[])
{
	if(argc < 4)
	{
		printf("Введите: %s начало_диапозона конец_диапозона max_чисел_в_диапозоне\n",argv[0]);
		exit(1);
	}

	//диапозон [a;b]
	int a = atoi(argv[1]);
	int b = atoi(argv[2]);
	//n - количество чисел анализируемых в 1 процессе
	int n = atoi(argv[3]);
	int s,e,count = (b - a + 1) / n;
  //Создаём сокет сервера
  int serv_sock = openSocket(PORT_TCP,count);
  pthread_t *tid = calloc(count,sizeof(pthread_t));
	s = a;
  int k = 0;

	do
	{
		e = s + n - 1;
		if( e > b) e = b;
    //Запуск клиентской программы
		if(!fork())
    {
      close(serv_sock);
      execl("./client", "./client",NULL);
    }

    //Ожидаем подключения клиента
    usleep(10);
    int client_sock = accept(serv_sock, NULL, 0);

    thread_data data;

    data.sock = client_sock;
    data.s = s;
    data.e = e;

    if(pthread_create(&tid[k], NULL, fthread, &data))
    {
      printf("Не удалось создать поток для диапозона [%d ; %d]\n",s,e);
    }
    k++;

		s = e + 1;
	} while(e < b);

  //Освобождаем ресурсы
	for(int i = 0; i < count; i++)
		pthread_join(tid[i], NULL);
  for(int i = 0; i < count; i++)
    wait(NULL);

  //Закрываем сокет сервера
  close(serv_sock);

  free(tid);
  printf("\n");

	return 0;
}
