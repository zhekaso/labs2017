
#define _SVID_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <pthread.h>

#include "join_functions.h"
#include "conf.h"

#include "message.pb-c.h"

//Константы
/*---------------------------*/
#define QUEUE_LEN 5
#define TCP_SOCKET_LISTEN_LEN 64
#define K_SECONDS 5
#define L_SECONDS 5
/*---------------------------*/

//Общие данные потоков
/*---------------------------*/
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
int curent_qlen;
struct queue Q_tasks = {NULL,NULL};
/*---------------------------*/


//Инициализация сокета для посылки UDP пакетов
int initSocket_UDP(int port)
{
	int sd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sd < 0)
		exitERR("Сервер: не удалось создать сокет UDP");

	struct sockaddr_in server_addr;
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port);
	server_addr.sin_addr.s_addr = INADDR_ANY;

	if(bind(sd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0)
		exitERR("Сервер: не удалось связать сокет UDP с адресом");

	return sd;
}

//инициализация сокета для TCP соединения
int initSocket_TCP(int port,int client_len)
{
	int on = 1;
	int sd = socket(AF_INET, SOCK_STREAM, 0);
	if(sd < 0)
		exitERR("Сервер: не удалось создать сокет TCP");

		struct sockaddr_in server_addr;
		memset(&server_addr, 0, sizeof(server_addr));
		server_addr.sin_family = AF_INET;
		server_addr.sin_port = htons(port);
		server_addr.sin_addr.s_addr = INADDR_ANY;
		setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof on);
		if(bind(sd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0)
			exitERR("Сервер: не удалось связать сокет TCP с адресом");

		if(listen(sd, client_len) < 0)
			exitERR("Сервер: не удалось начать прослушивать TCP сокет");

		return sd;

}

//Функция потока, которая посылает сообщение о том,
// что в очереди имеется задание
void *hasTask(void *in)
{
	int sUDP = *(int*)in;

	AMessage amsg;
	size_t len;

	struct sockaddr_in dest;
	memset(&dest, 0, sizeof(dest));
	dest.sin_family = AF_INET;
	dest.sin_port = htons(PORT_UDP_CLIENT2);
	dest.sin_addr.s_addr = inet_addr(ADDR);

	amessage__init(&amsg);
	amsg.type = MSG_HAS_TASK;
	len = amessage__get_packed_size(&amsg);
	void *buf = malloc(len);
	amessage__pack(&amsg, buf);

	while(1){

	pthread_mutex_lock(&lock);

	if(curent_qlen > 0)
	{
		sendto(sUDP,&len,sizeof(size_t),0,(struct sockaddr*)&dest,sizeof dest);
		sendto(sUDP,buf,len,0,(struct sockaddr*)&dest,sizeof dest);
		printf("Отправака запроса на выполнения задания >>> \n");
	}

	pthread_mutex_unlock(&lock);
	sleep(L_SECONDS);
	}
	free(buf);
	return NULL;
}

//Функция потока, которая посылает сообщение о том,
// что в очереди есть место для задания
void *waitingTask(void *in)
{
	int sUDP = *(int*)in;

	AMessage amsg;
	size_t len;

	struct sockaddr_in dest;
	memset(&dest, 0, sizeof(dest));
	dest.sin_family = AF_INET;
	dest.sin_port = htons(PORT_UDP_CLIENT1);
	dest.sin_addr.s_addr = INADDR_ANY;

	amessage__init(&amsg);
	amsg.type = MSG_WAITING_TASK;
	len = amessage__get_packed_size(&amsg);
	void *buf = malloc(len);
	amessage__pack(&amsg, buf);

	while(1){

	pthread_mutex_lock(&lock);

	if(curent_qlen < QUEUE_LEN)
	{
		sendto(sUDP,&len,sizeof(size_t),0,(struct sockaddr*)&dest,sizeof dest);
		sendto(sUDP,buf,len,0,(struct sockaddr*)&dest,sizeof dest);
		printf("Отправка запроса на получения задания >>> \n");
	}

	pthread_mutex_unlock(&lock);
	sleep(K_SECONDS);
	}
	free(buf);
	return NULL;
}

//Функция устаналивает TCP соединение с клиентской программой 1
void *getRandomMessage(void *in)
{
	int sd = *(int*)in;
	size_t len;
	void *buf;

	AMessage *gmsg;
	AMessage smsg = AMESSAGE__INIT;
	Task newtask;
	//Получаем задание
	recv(sd, &len, sizeof(size_t), 0);
	buf = malloc(len);
	recv(sd, buf, len , 0);

	pthread_mutex_lock(&lock);

	if(curent_qlen < QUEUE_LEN) //Если пока получали задание осталось место
	{
		//Добавляем задание в очередь
		gmsg = amessage__unpack(NULL, len, buf);
		newtask = *(gmsg->task);
		add(&Q_tasks, newtask);
		free(buf);
		buf = NULL;
		curent_qlen++;
		smsg.type = MSG_OK; //Статус: всё хорошо
		printf("NEWTASK: TIME: %d LEN: %d STR: %s <<< \n",newtask.time,newtask.len,newtask.str);
	}
	else
		smsg.type=MSG_BAD; //Статус: задание не добавлено

	pthread_mutex_unlock(&lock);
	len = amessage__get_packed_size(&smsg);
	if(buf) free(buf);
	buf = malloc(len);
	amessage__pack(&smsg,buf);
	send(sd,&len,sizeof(size_t),0);
	send(sd, buf, len, 0); //Отправляем статус
	free(buf);
	close(sd);
	free(in);
	return NULL;
}

//Функция рассылает задание клиентам 2
void *sendTask(void *in)
{
	int sd = *(int*)in;
	size_t len;
	void *buf;

	AMessage smsg = AMESSAGE__INIT;


	Task newtask;
	pthread_mutex_lock(&lock);

	if(curent_qlen > 0) //Если пока соединялись остались задания
	{

		get(&Q_tasks,&newtask);

		smsg.type = MSG_OK; //Статус: всё хорошо
		smsg.task = &newtask;
		len = amessage__get_packed_size(&smsg);
		buf = malloc(len);
		amessage__pack(&smsg, buf);

		send(sd, &len, sizeof(size_t), 0);
		send(sd, buf, len, 0); //Отправляем задание
		free(buf);
		curent_qlen--;
		printf("SEND: TIME: %d LEN: %d STR: %s >>> \n",newtask.time,newtask.len,newtask.str);
	}
	else
	{
		smsg.type = MSG_BAD;
		len = amessage__get_packed_size(&smsg);
		buf = malloc(len);
		amessage__pack(&smsg, buf);

		send(sd, &len, sizeof(size_t), 0);
		send(sd, buf, len, 0); //Отправляем cтатус, о занятом месте
		free(buf);
	}
	pthread_mutex_unlock(&lock);
	close(sd);
	free(in);
	return NULL;
}

int main()
{
	int sUDP = initSocket_UDP(PORT_UDP_SERVER);
	int sTCP = initSocket_TCP(PORT_TCP,TCP_SOCKET_LISTEN_LEN);

	//Создаём очередь

	curent_qlen = 0;

	pthread_t tid[3];
	pthread_create(&tid[0], NULL, waitingTask, &sUDP);
	pthread_create(&tid[1], NULL, hasTask, &sUDP);


	while(1)
	{
		void *buf;
		pthread_t tid;
		int *clsock = malloc(sizeof(int));
		AMessage *amsg;

		size_t len;

		*clsock = accept(sTCP, NULL, NULL);

		//Получаем сообщение с типом клиента
		recv(*clsock, &len,sizeof(size_t),0);
		buf = malloc(len);
		recv(*clsock, buf, len, 0);

		amsg = amessage__unpack(NULL, len, buf);

		if(amsg->type == MSG_CLIENT_TYPE_1) //Если присоединился пользователь 1 типа
		{
			pthread_create(&tid, NULL, getRandomMessage, clsock);
		}
		else //Если присоединился пользователь 2 типа
		{
			pthread_create(&tid, NULL, sendTask, clsock);
		}
		//Отсоединяем поток
		pthread_detach(tid);
	}

	pthread_join(tid[0], NULL);
	pthread_join(tid[1], NULL);

	clear(&Q_tasks);
	close(sTCP);
	close(sUDP);
	return 0;
}
