//#define _BSD_SOURCE
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <time.h>

#include "join_functions.h"
#include "conf.h"

#include "message.pb-c.h"

//Инициализация сокета для посылки UDP пакетов
int initSocket_UDP(int port)
{
	//int on = 1;
	int sd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sd < 0)
		exitERR("Клиент2: не удалось создать сокет UDP");

	struct sockaddr_in dest_addr;
	memset(&dest_addr, 0, sizeof(dest_addr));
	dest_addr.sin_addr.s_addr = INADDR_ANY;
	dest_addr.sin_family = AF_INET;
	dest_addr.sin_port = htons(port);

	//setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof on);
	while(bind(sd, (struct sockaddr*) &dest_addr, sizeof(dest_addr)) < 0);
	//if(bind(sd, (struct sockaddr*) &dest_addr, sizeof(dest_addr)) < 0)
		//exitERR("Клиент: не удалось привязать порт клиенту");

	return sd;
}

//Присоединяемся по TCP к серверу
int connectSocket_TCP(int port)
{
	int sd = socket(AF_INET, SOCK_STREAM, 0);
	if(sd < 0)
		exitERR("Клиент2: не удалось создать сокет TCP");

	struct sockaddr_in dest_addr;
	memset(&dest_addr, 0, sizeof(dest_addr));
	dest_addr.sin_addr.s_addr = inet_addr(ADDR);
	dest_addr.sin_family = AF_INET;
	dest_addr.sin_port = htons(port);

	if(connect(sd, (struct sockaddr*) &dest_addr, sizeof dest_addr) < 0)
		exitERR("Клиент2: не удалось присоединится к серверу");
	return sd;
}

int main()
{
		int sUDP;
		struct sockaddr_in serv;
		socklen_t addrlen = sizeof serv;
		AMessage amsg;
		AMessage *gmsg;
		size_t len;
		void *buf;
		while(1)
		{
		//Ждём сообщение от сервера
		sUDP = initSocket_UDP(PORT_UDP_CLIENT2);
		recvfrom(sUDP, &len, sizeof(size_t), 0, (struct sockaddr*) &serv, &addrlen);
		buf = malloc(len);
		recvfrom(sUDP, buf, len, 0, (struct sockaddr*) &serv, &addrlen);
		close(sUDP);
		gmsg = amessage__unpack(NULL, len, buf);
		free(buf);
		//Проверяем сообщение
		if(gmsg->type == MSG_HAS_TASK)
		{
      printf("Готов получить задание от сервера сервера <<< \n");
			//соединяемся с сервером по TCP
			int sTCP = connectSocket_TCP(PORT_TCP);

			amessage__init(&amsg);
			amsg.type = MSG_CLIENT_TYPE_2;
			len = amessage__get_packed_size(&amsg);
			buf = malloc(len);
			amessage__pack(&amsg, buf);
			send(sTCP, &len, sizeof(size_t), 0);
			send(sTCP, buf, len, 0);

			free(buf);
			Task newtask;
			recv(sTCP,&len,sizeof(size_t),0);
			buf = malloc(len);
			recv(sTCP,buf,len,0);
			amessage__free_unpacked(gmsg, NULL);
			gmsg = amessage__unpack(NULL, len, buf);

      if(gmsg->type == MSG_OK)
      {
        newtask = *(gmsg->task);

        printf("EXE: TIME: %d LEN: %d STR: %s <<< \n",newtask.time,newtask.len,newtask.str);

        printf("Выполняю....\n");
        sleep(newtask.time);

        printf("Закончил....\n");
      }
			else
				printf("Отмена выполнения задания <<< \n");

			amessage__free_unpacked(gmsg, NULL);
			free(buf);
			close(sTCP);
		}
    }
    return 0;
}
