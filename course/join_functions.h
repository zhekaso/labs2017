#include <stdio.h>
#include <stdlib.h>
#include "conf.h"
#include "message.pb-c.h"
struct s_task
{
  int time;
  int strlen;
  char str[STR_MAX_LEN+1];
};

typedef struct s_task task;

struct list
{
	Task el;
	struct list *next;
};

struct queue
{
	struct list *head;
	struct list *tail;
};

int add(struct queue *Q,Task el)
{

	struct list *new = malloc(sizeof(struct list));
	new->el = el;
	new->next = NULL;
	if(!Q->head)
	{
		Q->head = new;
		Q->tail = new;
		return 1;
	}
	Q->tail->next = new;
	Q->tail = new;
	return 1;
}

int get(struct queue *Q,Task *el)
{
	if(!Q->head) return 0;
	*el = Q->head->el;

	struct list *old = Q->head;
	Q->head = old->next;
	if(!old->next) Q->tail=NULL;
	return 1;
}

int clear(struct queue *Q)
{
	struct list *h;
	while(h)
	{
		struct list *p;
		p = h;
		h = h->next;
		free(p);
	}
	return 1;
}
void printQueu(struct queue Q)
{
  struct list *h = Q.head;
  while(h)
  {
  printf("TIME: %d",h->el.time);
  if(h->next) printf(" -> ");
    h = h->next;
  }
  printf("\n");
}
void exitERR(char* msg)
{
  fprintf(stderr, "%s\n", msg);
  exit(1);
}
