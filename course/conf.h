#ifndef CONF_H
#define CONF_H


//Общие константы клиентов и сервера
#define PORT_UDP_SERVER 2000

#define PORT_UDP_CLIENT1 2001
#define PORT_UDP_CLIENT2 2011

#define PORT_TCP 2012

#define ADDR "127.0.0.1"

#define MSG_WAITING_TASK 0x01
#define MSG_HAS_TASK 0x11

#define MSG_OK 0x03
#define MSG_BAD 0x04

#define MSG_CLIENT_TYPE_1 0x50
#define MSG_CLIENT_TYPE_2 0x10

#define MSG_SIZE sizeof(char)

#define STR_MIN_LEN 8
#define STR_MAX_LEN 32
#define MIN_TIME 1
#define MAX_TIME 10

#define BUFFER_SIZE 1024

#endif
