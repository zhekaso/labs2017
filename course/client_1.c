//#define _BSD_SOURCE
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <time.h>

#include "join_functions.h"
#include "conf.h"

#include "message.pb-c.h"

//Инициализация сокета для посылки UDP пакетов
int initSocket_UDP(int port)
{
	//int on = 1;
	int sd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sd < 0)
		exitERR("Клиент1: не удалось создать сокет UDP");

	struct sockaddr_in dest_addr;
	memset(&dest_addr, 0, sizeof(dest_addr));
	dest_addr.sin_addr.s_addr = INADDR_ANY;
	dest_addr.sin_family = AF_INET;
	dest_addr.sin_port = htons(port);

	//setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof on);
	while(bind(sd, (struct sockaddr*) &dest_addr, sizeof(dest_addr)) < 0);
	/*if(bind(sd, (struct sockaddr*) &dest_addr, sizeof(dest_addr)) < 0)
		exitERR("Клиент: не удалось привязать порт клиенту");*/

	return sd;
}

int connectSocket_TCP(int port)
{
	int sd = socket(AF_INET, SOCK_STREAM, 0);
	if(sd < 0)
		exitERR("Клиент1: не удалось создать сокет TCP");

	struct sockaddr_in dest_addr;
	memset(&dest_addr, 0, sizeof(dest_addr));
	dest_addr.sin_addr.s_addr = inet_addr(ADDR);
	dest_addr.sin_family = AF_INET;
	dest_addr.sin_port = htons(port);

	connect(sd, (struct sockaddr*) &dest_addr, sizeof dest_addr);
	return sd;
}

//Генерирую рандомную строку и время
int genetateTask(char** str, int* len)
{
	int t;
	srand((unsigned) time(NULL));
	t = rand() % MAX_TIME + MIN_TIME;
	*len = rand() % STR_MAX_LEN + STR_MIN_LEN;
	*str = malloc(*len);
	for(int i = 0; i < *len;i++)
	{
		(*str)[i] = rand() % ('z'-'a') + 'a';
	}
	(*str)[*len] = '\0';
	return t;
}

int main()
{
		int sUDP;
		int t_seconds;
		struct sockaddr_in serv;
		socklen_t addrlen = sizeof(serv);
		size_t len;
		AMessage amsg = AMESSAGE__INIT;
		void *buf;
		Task newtask;
		while(1)
		{
		//Ждём сообщение от сервера
		sUDP = initSocket_UDP(PORT_UDP_CLIENT1);
		recvfrom(sUDP, &len, sizeof(size_t), 0, (struct sockaddr*) &serv, &addrlen);
		buf = malloc(len);
		recvfrom(sUDP, buf, len, 0, (struct sockaddr*) &serv, &addrlen);
		close(sUDP);
		AMessage *gmsg = amessage__unpack(NULL, len, buf);
		free(buf);
		close(sUDP);

		//Проверяем сообщение
		if(gmsg->type == MSG_WAITING_TASK)
		{
			printf("Получил запрос от сервера на задание <<<\n");
			//соединяемся с сервером по TCP
			int sTCP = connectSocket_TCP(PORT_TCP);

			amessage__init(&amsg);
			//AMessage amsg = AMESSAGE__INIT;
			amsg.type = MSG_CLIENT_TYPE_1;
			len = amessage__get_packed_size(&amsg);
			buf = malloc(len);
			amessage__pack(&amsg, buf);
			send(sTCP, &len, sizeof(size_t), 0);
			send(sTCP, buf, len, 0);
			free(buf);
			task__init(&newtask);
			t_seconds = genetateTask(&newtask.str, &newtask.len);
			
			newtask.time = t_seconds;
			amessage__init(&amsg);

			amsg.type=MSG_OK;
			amsg.task=&newtask;

			len = amessage__get_packed_size(&amsg);
			buf = malloc(len);
			amessage__pack(&amsg, buf);
			send(sTCP, &len, sizeof(size_t), 0);
			send(sTCP,	buf, len, 0);
			free(buf);
			recv(sTCP, &len, sizeof(size_t), 0);
			buf = malloc(len);
			recv(sTCP, buf, len, 0);
			amessage__free_unpacked(gmsg, NULL);
			gmsg = amessage__unpack(NULL, len, buf);
			free(buf);
			close(sTCP);
		}
		if(gmsg->type == MSG_OK)
			{
				printf("SEND: TIME: %d LEN: %d STR: %s >>> \n",newtask.time,newtask.len,newtask.str);
				sleep(t_seconds);
			}
		amessage__free_unpacked(gmsg, NULL);
		}

    return 0;
}
