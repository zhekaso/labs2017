
#include <stdio.h>
#include "Mpow.h"

int main()
{
	int p3 = Mpow3(2);
	int p4 = Mpow4(2);
	printf("2^3 = %d\n",p3);
	printf("2^4 = %d\n",p4);
	return 0;
}
