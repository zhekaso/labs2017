#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

typedef long (*fpow)(int);

int main()
{
	void* dllhandler = dlopen("libMpow.so", RTLD_LAZY);
	if(!dllhandler)
	{
		printf("Не удалось найти динамическую библеотеку: libMpow.so\n");
		exit(1);
	}
	//fpow m;
	fpow Mpow3 = NULL;
	fpow Mpow4 = NULL;
	Mpow3 = dlsym(dllhandler, "Mpow3");
	Mpow4 = dlsym(dllhandler,"Mpow4");
	if(!Mpow3)
	{
		printf("Не удалось загрузить функцию: Mpow3\n");
		dlclose(dllhandler);
		exit(1);
	}
	int p3 = Mpow3(2);
	printf("2^3 = %d\n",p3);

	if(!Mpow4)
	{
		printf("Не удалось загрузить функцию: Mpow4\n");
		dlclose(dllhandler);
		exit(1);
	}
	int p4 = Mpow4(2);
	printf("2^4 = %d\n",p4);
	dlclose(dllhandler);
	return 0;
}
