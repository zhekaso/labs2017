#include "Mpow.h"

long Mpow(int num,int rang)
{
	long result = 1;
	while(rang > 0)
	{
		result *= num;
		rang--;;
	}
	return result;
}

long Mpow3(int num)
{
	return Mpow(num,3);
}

long Mpow4(int num)
{
	return Mpow(num,4);
}
