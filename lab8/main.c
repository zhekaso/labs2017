
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <fcntl.h>
#include <sys/types.h>
//#include <sys/ipc.h>
#include <sys/msg.h>

#define CODE_FILE "/tmp/code_l8"
#define MAX_MSG_LEN 1024

typedef struct
{
  long mtype;
  int msg[2];
} s_msg;

int openMSG(key_t key)
{
  int id = msgget(key,IPC_CREAT | IPC_EXCL |  0666);
  if(id == -1)
  {
    id = msgget(key, IPC_EXCL);
    if(id == -1)
    {
      printf("Не удалось создать или открыть очередь\n");
      exit(1);
    }
    else return id;
  }
  else return id;
}

void rmMSG(int id)
{
  if(msgctl(id, IPC_RMID, NULL) == -1 )
  {
    printf("Error: не удалось закрыть очередь\n");
  }
}

int nod(int a, int b)
{
	while(a != b)
	{
		if(a > b)
			a = a - b;
		else b = b - a;
	}
	return a;
}

int primenumber(int num)
{
	for(int i = 2; i <= (int)sqrt(num) + 1;i++)
	{
		int NOD = nod(num,i);
		if( NOD != 1 && NOD != num) return 0;
	}
	return 1;
}

int main(int argc,char* argv[])
{

	if(argc < 4)
	{
		printf("Введите: %s начало_диапозона конец_диапозона max_чисел_в_диапозоне\n",argv[0]);
		exit(1);
	}
	//диапозон [a;b]
	int a = atoi(argv[1]);
	int b = atoi(argv[2]);
	//n - количество чисел анализируемых в 1 процессе
	int n = atoi(argv[3]);
	int s,e,count = 0;
	s = a;

  unlink(CODE_FILE);
  close(creat(CODE_FILE, 0666));
  int msgID = openMSG(ftok(CODE_FILE, 2));

	do
	{
		e = s + n - 1;
		if( e > b) e = b;
		count++;

    s_msg msg = {1, {s,e}};

    msgsnd(msgID, &msg, 2 * sizeof(int), 0);
		if(!fork())
		{
			int s,e;
      int msgID = openMSG(ftok(CODE_FILE, 2));
      s_msg msg;
      msgrcv(msgID, &msg, 2 * sizeof(int), 1, 0);
      s = msg.msg[0];
      e = msg.msg[1];
      //printf("\n[%d : %d]\n",s,e);
			for(int i = s; i <= e; i++)
			{
				if(primenumber(i)) printf("%d ",i);
			}
			exit(0);
		}
		s = e + 1;
	} while(e < b);

	for(int i = 0; i < count; i++)
		wait(NULL);
  rmMSG(msgID);
	printf("\n");
	return 0;
}
