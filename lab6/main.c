
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>

int nod(int a, int b)
{
	while(a != b)
	{
		if(a > b)
			a = a - b;
		else b = b - a;
	}
	return a;
}

int primenumber(int num)
{
	for(int i = 2; i <= (int)sqrt(num) + 1;i++)
	{
		int NOD = nod(num,i);
		if( NOD != 1 && NOD != num) return 0;
	}
	return 1;
}


int main(int argc,char* argv[])
{
	if(argc < 4)
	{
		printf("Введите: %s начало_диапозона конец_диапозона max_чисел_в_диапозоне\n",argv[0]);
		exit(1);
	}
	//диапозон [a;b]
	int a = atoi(argv[1]);
	int b = atoi(argv[2]);
	//n - количество чисел анализируемых в 1 процессе
	int n = atoi(argv[3]);
	int s,e,count = 0;
	s = a;
	clock_t t = clock();
	do
	{
		e = s + n - 1;
		if( e > b) e = b;
		count++;

		if(!fork())
		{
			for(int i = s; i <= e; i++)
			{
				if(primenumber(i)) printf("%d ",i);
			}
			exit(0);
		}
		s = e + 1;
	} while(e < b);
	for(int i = 0; i < count; i++)
		wait(NULL);
	t = clock() - t;
	printf("\nВремя выполнения %ld\n",t);

	return 0;
}
