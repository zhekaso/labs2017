
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#define FIFO_PATH "/tmp/fifo_l6"

int nod(int a, int b)
{
	while(a != b)
	{
		if(a > b)
			a = a - b;
		else b = b - a;
	}
	return a;
}

int primenumber(int num)
{
	for(int i = 2; i <= (int)sqrt(num) + 1;i++)
	{
		int NOD = nod(num,i);
		if( NOD != 1 && NOD != num) return 0;
	}
	return 1;
}

int main(int argc,char* argv[])
{
	if(argc < 4)
	{
		printf("Введите: %s начало_диапозона конец_диапозона max_чисел_в_диапозоне\n",argv[0]);
		exit(1);
	}
	//диапозон [a;b]
	int a = atoi(argv[1]);
	int b = atoi(argv[2]);
	//n - количество чисел анализируемых в 1 процессе
	int n = atoi(argv[3]);
	int s,e,count = 0;
	s = a;
	unlink(FIFO_PATH);
	if(mkfifo(FIFO_PATH, 0666))
	{
		printf("Не удалось создать именованный канал\n");
		exit(0);
	}
	int fd = open(FIFO_PATH,O_RDWR);
	if(fd < 0)
	{
		printf("Не удалось открыть именнованый канал\n");
		exit(1);
	}
	do
	{
		e = s + n - 1;
		if( e > b) e = b;
		count++;
		write(fd, &s, sizeof(int));
		write(fd, &e, sizeof(int));
		if(!fork())
		{
			int s,e;
			int fd = open(FIFO_PATH,O_RDONLY);
			if(fd < 0)
			{
				printf("Не удалось открыть именнованый канал\n");
				exit(1);
			}
			read(fd, &s, sizeof(int));
			read(fd, &e, sizeof(int));
			for(int i = s; i <= e; i++)
			{
				if(primenumber(i)) printf("%d ",i);
			}
			close(fd);
			exit(0);
		}
		s = e + 1;
	} while(e < b);
	for(int i = 0; i < count; i++)
		wait(NULL);
	printf("\n");
	close(fd);
	return 0;
}
