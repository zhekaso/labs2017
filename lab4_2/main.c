/*7
Удалить из текста все пробелы
Параметры командной строки:
	1. Имя входного файла
	2. Количество замен */

	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>

	#define NAME_LEN 64
	#define MAX_LEN 1024


//Меняет имя тип файла на "mod"
void changeType(char* new_name,char* old_name)
{
		int i;
		for(i = 0; old_name[i] != '.' && old_name != '\0'; i++)
		{
			new_name[i] = old_name[i];
		}
		new_name[i] = '.';
		i++;
		strcpy(&new_name[i], "mod\0");
}

//Копирует файл fin в fout, без пробелов но не больше n_swaps
void removeSpaces(char* fin, char* fout, int n_swaps)
{
	FILE *fw,*fr;
	int n=0;
	if( (fr = fopen(fin, "r")) == NULL )
	{
		printf("Не удалось открыть файл %s!\n",fin);
		exit(1);
	}
	if( (fw = fopen(fout, "w")) == NULL )
	{
		printf("Не удалось открыть файл %s!\n",fout);
		fclose(fr);
		exit(1);
	}
	char ch;
	while((ch = fgetc(fr)) != EOF)
	{
		if(ch != ' ' || n >= n_swaps)
		{
			n++;
			fputc(ch,fw);
		}
	}
	fseek(fw, -1, SEEK_CUR);
	fprintf(fw, "%c", '\0');
	fclose(fw);
	fclose(fr);
}
int main(int argc, char* argv[])
{
	if(argc < 2)
	{
		printf("Ошибка: ожидается %s имя_файла количество_замен\n",argv[0]);
		exit(1);
	}
	char fname[NAME_LEN];
	int count_swaps = atoi(argv[2]);
	changeType(fname, argv[1]);
	removeSpaces(argv[1],fname,count_swaps);
	return 0;
}
